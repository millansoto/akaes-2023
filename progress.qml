import QtQuick 2.12
import QtQuick.Controls 2.12

ProgressBar {
    id: progressBar

    background: Rectangle {
        color: "lightGray"
    }

    palette.dark: "darkGray"

    Text {
        z: 2
        font.weight: Font.ExtraBold
        objectName: "progressLabel"
        color: "blue"
        anchors.centerIn: parent
    }
}
