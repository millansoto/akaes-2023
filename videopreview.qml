import QtQuick 2.15
import QtQuick.Controls 2.15

Rectangle {
    width: 400
    height: 400

    Image {
        id: thumbnailImage
        objectName: "thumbnailImage"
        //source: "your_image_path_here.jpg" // replace with your image path
        anchors.fill: parent
        fillMode: Image.PreserveAspectFit
    }

    Button {
        id: downloadButton
        objectName: "downloadButton"
        text: "Download"
        anchors.bottom: parent.bottom
        anchors.rightMargin: 20
        anchors.bottomMargin: 20
        anchors.right: parent.right

        onClicked: {
            dialog.download();
        }
    }
}
