#!/usr/bin/env python
# Copyright (C) 2019-2024 José Millán Soto <jmillan (at) kde-espana (dot) org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

import os
import sys

import yt_dlp
from yt_dlp.utils import DownloadError, YoutubeDLError

from dialog import Ui_Dialog

from PyQt6.QtCore import QThread, QUrl, pyqtSignal, pyqtSlot
from PyQt6.QtQuick import QQuickItem
from PyQt6.QtWidgets import QApplication, QDialog, QFileDialog, QMessageBox


class GetFormatsThread(QThread):
    def __init__(self, video_url, parent=None):
        super().__init__(parent)
        self.video_url = video_url
        self.ydl = yt_dlp.YoutubeDL()

    def run(self):
        try:
            self.result = self.ydl.extract_info(self.video_url, download=False)
        except YoutubeDLError as exc:
            self.result = {'exception': exc}


class Downloader(yt_dlp.YoutubeDL):
    def __init__(self, thread, *args, **kwargs):
        self.thread = thread
        self.fileName = None
        self._dicts = []
        super().__init__(*args, **kwargs)

    def setFilename(self, fileName):
        self.fileName = fileName

    def prepare_filename(self, info_dict, warn=True):
        if self.fileName:
            return self.fileName
        default = super().prepare_filename(info_dict)
        complete_default = os.path.join(os.getcwd(), default)
        self.thread.requestName(complete_default)

        while self.fileName is None:
            QThread.yieldCurrentThread()
        return self.fileName


class DownloadThread(QThread):
    nameRequested = pyqtSignal(str)
    #params: downloaded, total, rate
    downloadStatus = pyqtSignal(int, int, int)
    #params: success
    downloadFinished = pyqtSignal(bool)

    def __init__(self, video_url, format_id=None, parent=None):
        super().__init__(parent)
        self.video_url = video_url
        params = {}
        if format_id:
            params['format'] = format_id
        self.ydl = Downloader(self, params)
        self.ydl.add_progress_hook(lambda status: self.checkStatus(status))
        self.format_id = format_id

    def checkStatus(self, status):
        if status['status'] == 'downloading':
            total_bytes = status.get('total_bytes', status.get('total_bytes_estimate', status['downloaded_bytes']))
            self.downloadStatus.emit(status['downloaded_bytes'], total_bytes, status['speed'])

    def requestName(self, default_name):
        self.nameRequested.emit(default_name)

    def gotFileName(self, fileName):
        self.ydl.setFilename(fileName)

    def run(self):
        try:
            self.ydl.download([self.video_url])
            self.downloadFinished.emit(True)
        except DownloadError:
            self.downloadFinished.emit(False)


class VideoDownload(QDialog):
    @pyqtSlot()
    def download(self):
        video_url = self.ui.urlEdit.text()
        self._qmlDownloadButton().setEnabled(False)
        self.ui.urlEdit.setEnabled(False)
        format_id = self.ui.formatSelection.currentData()
        self.thread = DownloadThread(video_url, format_id, self)
        self.thread.nameRequested.connect(self.nameRequested)
        self.thread.finished.connect(self.enableUi)
        self.thread.downloadStatus.connect(self.downloadStatus)
        self.thread.downloadFinished.connect(self.downloadFinished)
        self.thread.start()

    def nameRequested(self, default_name):
        sender = self.sender()
        sender.gotFileName(QFileDialog.getSaveFileName(None, 'Download output file', default_name)[0])

    def __rateText(self, rate):
        if rate < 2048:
            return '%s b/s' % rate
        if rate < 2097152:
            return '%.2f KiB/s' % (rate / 1024)
        return '%.2f MiB/s' % (rate / 1048576)

    def _qmlProgressBar(self):
        return self.ui.progressBar.rootObject()

    def _qmlProgressLabel(self):
        return self.ui.progressBar.rootObject().findChild(QQuickItem, 'progressLabel')

    def _qmlThumbnailImage(self):
        return self.ui.videoThumbnail.rootObject().findChild(QQuickItem, 'thumbnailImage')

    def _qmlDownloadButton(self):
        return self.ui.videoThumbnail.rootObject().findChild(QQuickItem, 'downloadButton')

    def downloadStatus(self, downloaded, total, rate):
        qml_progressBar = self._qmlProgressBar()
        qml_progressLabel = self._qmlProgressLabel()
        qml_progressBar.setProperty('to', float(total))
        qml_progressBar.setProperty('value', float(downloaded))
        qml_progressLabel.setProperty('text', self.__rateText(rate))
        self.ui.progressBar.show()

    def downloadFinished(self, success):
        qml_progressBar = self._qmlProgressBar()
        qml_progressLabel = self._qmlProgressLabel()
        qml_progressBar.setProperty('to', 1)
        if success:
            qml_progressBar.setProperty('value', 1)
            qml_progressLabel.setProperty('text', 'Done!')
            QMessageBox.information(self, "Download finished", "Download finished successfully")
        else:
            qml_progressBar.setProperty('value', 0)
            QMessageBox.critical(self, "Download failed", "Download finished unsuccessfully")

    def getFormats(self):
        video_url = self.ui.urlEdit.text()
        self._qmlDownloadButton().setEnabled(False)
        self.ui.urlEdit.setEnabled(False)
        self.ui.formatSelection.setEnabled(False)
        qml_progressBar = self._qmlProgressBar()
        qml_progressLabel = self._qmlProgressLabel()
        qml_progressBar.setProperty('indeterminate', True)
        qml_progressLabel.setProperty('text', 'Obtaining available formats...')
        self.ui.progressBar.show()
        self.thread = GetFormatsThread(video_url, self)
        self.thread.finished.connect(self.infoObtained)
        self.thread.start()

    def enableUi(self):
        self.ui.formatSelection.setEnabled(True)
        self.ui.urlEdit.setEnabled(True)
        self._qmlDownloadButton().setEnabled(True)

    def infoObtained(self):
        self.ui.progressBar.hide()
        self.ui.formatSelection.clear()
        if 'exception' in self.thread.result:
            QMessageBox.critical(self, "Could not obtain formats", "Could not obtain formats for the selected video, please ensure the URL is correct")
            return
        self.ui.formatSelection.addItem('default', None)
        if 'formats' in self.thread.result:
            for i in self.thread.result['formats']:
                self.ui.formatSelection.addItem(i['format'], i['format_id'])
        thumbnails = self.thread.result.get('thumbnails')
        if thumbnails:
            thumbnails.sort(key=lambda x: x.get('width', 1) * x.get('height', 1), reverse=True)
            thumbnail_url = thumbnails[0]['url']
            self._qmlThumbnailImage().setProperty('source', thumbnail_url)
        qml_progressBar = self._qmlProgressBar()
        qml_progressBar.setProperty('indeterminate', False)
        self.ui.videoThumbnail.show()
        self.enableUi()

    def __init__(self):
        super().__init__()

        self.ui = Ui_Dialog()
        self.ui.setupUi(self)

        qml_progress_url = QUrl(os.path.join(os.path.dirname(__file__), "progress.qml"))
        qml_videopreview_url = QUrl(os.path.join(os.path.dirname(__file__), "videopreview.qml"))
        self.ui.progressBar.setSource(qml_progress_url)
        self.ui.progressBar.hide()
        self.ui.videoThumbnail.setSource(qml_videopreview_url)
        self.ui.videoThumbnail.rootContext().setContextProperty("dialog", self)
        self.ui.videoThumbnail.hide()
        self.ui.formatsBtn.clicked.connect(self.getFormats)


app = QApplication(sys.argv)
dialog = VideoDownload()
dialog.show()
sys.exit(app.exec())
